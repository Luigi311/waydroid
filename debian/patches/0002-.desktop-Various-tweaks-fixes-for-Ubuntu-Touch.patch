From b97600ad9365f7b3892b1026eef85e444c8cb6b9 Mon Sep 17 00:00:00 2001
From: Jami Kettunen <jami.kettunen@protonmail.com>
Date: Tue, 7 Feb 2023 04:25:37 +0200
Subject: [PATCH 2/2] *.desktop: Various tweaks/fixes for Ubuntu Touch

- Waydroid.desktop: Hide by default until initialized
  Later we want to patch the first-launch to bring the user to System
  Settings where "waydroid init", hiding the icon etc functionality will
  be integrated as a plugin.
- Waydroid.desktop: Hardcode absolute icon path
  For some reason Lomiri can't find it even after rebooting despite suru
  inheriting from hicolor icon theme:
  $ grep Inherits /usr/share/icons/suru/index.theme
  Inherits=Humanity,ubuntu-mobile,hicolor,gnome
- *.desktop: Limit orientation to portait on Lomiri
  Trying to rotate the whole Waydroid window doesn't work and just
---
 data/Waydroid.desktop          |  1 +
 tools/actions/initializer.py   |  4 ++++
 tools/services/user_manager.py | 41 ++++++++++++++++++----------------
 3 files changed, 27 insertions(+), 19 deletions(-)

diff --git a/data/Waydroid.desktop b/data/Waydroid.desktop
index 5a54453..1ff2e0e 100644
--- a/data/Waydroid.desktop
+++ b/data/Waydroid.desktop
@@ -4,3 +4,4 @@ Name=Waydroid
 Exec=waydroid first-launch
 Icon=waydroid
 X-Purism-FormFactor=Workstation;Mobile;
+NoDisplay=true
diff --git a/tools/actions/initializer.py b/tools/actions/initializer.py
index ed885f5..d276f8d 100644
--- a/tools/actions/initializer.py
+++ b/tools/actions/initializer.py
@@ -3,6 +3,7 @@
 import logging
 import os
 from tools import helpers
+from tools.services.user_manager import makeWaydroidDesktopFile
 import tools.config
 
 import sys
@@ -151,6 +152,9 @@ def init(args):
                     initializer_service.Done()
             except dbus.DBusException:
                 pass
+
+        args.apps_dir = "/home/phablet/.local/share/applications" # required hack for UT since this runs as root
+        makeWaydroidDesktopFile(args, False)
     else:
         logging.info("Already initialized")
 
diff --git a/tools/services/user_manager.py b/tools/services/user_manager.py
index 5e03d25..13d38c4 100644
--- a/tools/services/user_manager.py
+++ b/tools/services/user_manager.py
@@ -9,6 +9,25 @@ from tools.interfaces import IPlatform
 
 stopping = False
 
+def makeWaydroidDesktopFile(args, hide):
+    desktop_file_path = args.apps_dir + "/Waydroid.desktop"
+    if os.path.isfile(desktop_file_path):
+        os.remove(desktop_file_path)
+    lines = ["[Desktop Entry]", "Type=Application"]
+    lines.append("Name=Waydroid")
+    lines.append("Exec=waydroid show-full-ui")
+    lines.append("X-Purism-FormFactor=Workstation;Mobile;")
+    lines.append("X-Lomiri-Supported-Orientations=portrait;")
+    if hide:
+        lines.append("NoDisplay=true")
+    lines.append("Icon=/usr/share/icons/hicolor/512x512/apps/waydroid.png")
+    desktop_file = open(desktop_file_path, "w")
+    for line in lines:
+        desktop_file.write(line + "\n")
+    desktop_file.close()
+    os.chown(desktop_file_path, 32011, 32011) # phablet user on Ubuntu Touch
+    os.chmod(desktop_file_path, 0o755)
+
 def start(args, session, unlocked_cb=None):
 
     def makeDesktopFile(appInfo):
@@ -28,6 +47,7 @@ def start(args, session, unlocked_cb=None):
             lines.append("Exec=waydroid app launch " + packageName)
             lines.append("Icon=" + args.waydroid_data + "/icons/" + packageName + ".png")
             lines.append("X-Purism-FormFactor=Workstation;Mobile;")
+            lines.append("X-Lomiri-Supported-Orientations=portrait;")
             desktop_file = open(desktop_file_path, "w")
             for line in lines:
                 desktop_file.write(line + "\n")
@@ -35,23 +55,6 @@ def start(args, session, unlocked_cb=None):
             os.chmod(desktop_file_path, 0o755)
             return 0
 
-    def makeWaydroidDesktopFile(hide):
-        desktop_file_path = args.apps_dir + "/Waydroid.desktop"
-        if os.path.isfile(desktop_file_path):
-            os.remove(desktop_file_path)
-        lines = ["[Desktop Entry]", "Type=Application"]
-        lines.append("Name=Waydroid")
-        lines.append("Exec=waydroid show-full-ui")
-        lines.append("X-Purism-FormFactor=Workstation;Mobile;")
-        if hide:
-            lines.append("NoDisplay=true")
-        lines.append("Icon=waydroid")
-        desktop_file = open(desktop_file_path, "w")
-        for line in lines:
-            desktop_file.write(line + "\n")
-        desktop_file.close()
-        os.chmod(desktop_file_path, 0o755)
-
     def userUnlocked(uid):
         logging.info("Android with user {} is ready".format(uid))
         args.waydroid_data = session["waydroid_data"]
@@ -68,9 +71,9 @@ def start(args, session, unlocked_cb=None):
                 makeDesktopFile(app)
             multiwin = platformService.getprop("persist.waydroid.multi_windows", "false")
             if multiwin == "false":
-                makeWaydroidDesktopFile(False)
+                makeWaydroidDesktopFile(args, False)
             else:
-                makeWaydroidDesktopFile(True)
+                makeWaydroidDesktopFile(args, True)
         if unlocked_cb:
             unlocked_cb()
 
-- 
2.39.1

